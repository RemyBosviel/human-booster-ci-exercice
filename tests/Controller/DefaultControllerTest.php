<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testHelloWorld()
    {
        $client = static::createClient();

        $client->request('GET', '/');

        $this->assertEquals(500, $client->getResponse()->getStatusCode());
    }
}
